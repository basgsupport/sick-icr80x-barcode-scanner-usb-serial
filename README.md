---

## About

This is a sample code for interfacing with [SICK ICR80x](https://www.sick.com/ae/en/identification-solutions/image-based-code-readers/icr80x/icr803-b0201/p/p93642) barcode scanner. 


---

## How to use

The interface use serial communication through USB.
Please download and install [TF6340 Serial Communication](https://www.beckhoff.com/english/download/tc3-download-tf6xxx.htm) from Beckhoff website.



---

## Details

TwinCAT will open serial connection to the SICK device. We suggest using serial terminal (e.g. Hyperterminal / Hercules) first to check the serial connection to the SICK barcode scanner.

To trigger the scanner:
Send "<SYN>T<CR>"
In hexadecimal, this is "16 54 0D"

To turn off the scanner:
Send "<SYN>U<CR>"
In hexadecimal, this is "16 55 0D"

There is a function inside the code that will parse this data. Please look at the comments in the code.


---

## Help

Please contact `support@beckhoff.com.sg`